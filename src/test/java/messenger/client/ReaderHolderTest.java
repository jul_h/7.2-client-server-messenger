package messenger.client;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ReaderHolderTest {
    @Test
    public void testGetReader() {
        BufferedReader readerMock = new BufferedReader(new StringReader("Sample input"));

        ReaderHolder readerHolder = new ReaderHolder();
        readerHolder.setReader(readerMock);

        BufferedReader reader = readerHolder.getReader();

        assertEquals(readerMock, reader);
    }

    @Test
    public void testGetReaderWithNullReader() {
        ReaderHolder readerHolder = new ReaderHolder();

        BufferedReader reader = readerHolder.getReader();

        assertNull(reader);
    }

    @Test
    public void testSetReader() {
        BufferedReader readerMock = new BufferedReader(new StringReader("Sample input"));
        ReaderHolder readerHolder = new ReaderHolder();

        readerHolder.setReader(readerMock);
        BufferedReader reader = readerHolder.getReader();

        assertEquals(readerMock, reader);
    }

    @Test
    public void testSetReaderWithNullReader() {
        BufferedReader readerMock = null;
        ReaderHolder readerHolder = new ReaderHolder();

        readerHolder.setReader(readerMock);
        BufferedReader reader = readerHolder.getReader();

        assertNull(reader);
    }
}


