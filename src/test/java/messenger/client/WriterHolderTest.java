package messenger.client;

import org.junit.jupiter.api.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class WriterHolderTest {
    @Test
    public void testGetWriter() {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writerMock = new PrintWriter(stringWriter);

        WriterHolder writerHolder = new WriterHolder();
        writerHolder.setWriter(writerMock);

        PrintWriter writer = writerHolder.getWriter();

        assertEquals(writerMock, writer);
    }

    @Test
    public void testGetWriterWithNullWriter() {
        WriterHolder writerHolder = new WriterHolder();

        PrintWriter writer = writerHolder.getWriter();

        assertNull(writer);
    }

    @Test
    public void testSetWriter() {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writerMock = new PrintWriter(stringWriter);
        WriterHolder writerHolder = new WriterHolder();

        writerHolder.setWriter(writerMock);
        PrintWriter writer = writerHolder.getWriter();

        assertEquals(writerMock, writer);
    }

    @Test
    public void testSetWriterWithNullWriter() {
        PrintWriter writerMock = null;
        WriterHolder writerHolder = new WriterHolder();

        writerHolder.setWriter(writerMock);
        PrintWriter writer = writerHolder.getWriter();

        assertNull(writer);
    }
}
