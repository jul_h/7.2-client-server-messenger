package messenger.client;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ClientTest {
    @Mock
    private Socket clientSocket;
    @Mock
    private PrintWriter writer;
    @Mock
    private BufferedReader reader;
    @Mock
    private MessageReceiver messageReceiver;
    @Mock
    private WriterHolder writerHolder;
    @Mock
    private ReaderHolder readerHolder;

    private Client client;

    @BeforeEach
    void setUp() {
        client = new Client(writerHolder, readerHolder, messageReceiver);
    }

    @Test
    public void testConnectToServer() throws IOException {
        // Arrange
        String expectedUsername = "John";

        when(clientSocket.isConnected()).thenReturn(true);
        doReturn(writer).when(writerHolder).getWriter();
        doReturn(reader).when(readerHolder).getReader();

        // Act
        client.connectToServer("localhost", 9999, expectedUsername);

        // Assert
        verify(writer).println(expectedUsername);
        verify(messageReceiver).startReceiving();
        verify(reader).close();
        verify(writer).close();
        verify(clientSocket).close();
    }

    @Test
    public void testGetMessageReceiver() {
        // Act
        MessageReceiver result = client.getMessageReceiver();

        // Assert
        assertNotNull(result);
        assertEquals(messageReceiver, result);
    }
}
