package messenger.client;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MessageReceiverTest {
    // Test startReceiving() method with a mock BufferedReader that returns a non-null message
    @Test
    public void testStartReceivingWithNonNullMessage() throws IOException {
        BufferedReader readerMock = Mockito.mock(BufferedReader.class);
        Mockito.when(readerMock.readLine()).thenReturn("Hello");

        MessageReceiver messageReceiver = new MessageReceiver(readerMock);
        messageReceiver.startReceiving();

        String message = messageReceiver.getMessage();
        assertEquals("Hello", message);
    }

    // Test startReceiving() method with a mock BufferedReader that returns null
    @Test
    public void testStartReceivingWithNullMessage() throws IOException {
        BufferedReader readerMock = Mockito.mock(BufferedReader.class);
        Mockito.when(readerMock.readLine()).thenReturn(null);

        MessageReceiver messageReceiver = new MessageReceiver(readerMock);
        messageReceiver.startReceiving();

        String message = messageReceiver.getMessage();
        assertNull(message);
    }

    // Test getMessage() method when message is null
    // Indirect verification for private setMessage()
    @Test
    public void testGetMessageWithNullMessage() {
        MessageReceiver messageReceiver = new MessageReceiver(null);
        String message = messageReceiver.getMessage();
        assertNull(message);
    }

    @Test
    public void testConstructor() {
        BufferedReader readerMock = Mockito.mock(BufferedReader.class);
        MessageReceiver messageReceiver = new MessageReceiver(readerMock);
        String message = messageReceiver.getMessage();
        assertNull(message);
    }
}
