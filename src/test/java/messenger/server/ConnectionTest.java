package messenger.server;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConnectionTest {

    private Connection connection;
    private Socket socket;

    @Before
    public void setUp() throws IOException {
        socket = new MockSocket();
        connection = new Connection(socket, null);
    }

    @Test
    public void testGetCustomerName() {
        String expectedName = "John";
        connection.setCustomerName(expectedName);

        String actualName = connection.getCustomerName();

        assertEquals(expectedName, actualName);
    }

    @Test
    public void testGetReceivedMessage() {
        String expectedMessage = "Hello";
        connection.setReceivedMessage(expectedMessage);

        String actualMessage = connection.getReceivedMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void testSendMessage() {
        String message = "Hello";

        connection.sendMessage(message);

        assertEquals(message, ((MockSocket)socket).getSentMessage());
    }

}
