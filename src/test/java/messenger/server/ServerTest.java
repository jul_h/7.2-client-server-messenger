package messenger.server;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

import static org.junit.Assert.*;


public class ServerTest {
    private Server server;

    @BeforeEach
    public void setUp() {
        server = new Server();
    }

    @Test
    public void testStart() {
        server.start();

        // Validate that the server is running
        assertTrue(server.isServerRunning());
    }

    @Test
    public void testStop() {
        server.start();
        server.stop();
    }

    @Test
    public void testSendMessageToAllClients() {
        server.start();

        // Create a dummy connection
        Connection connection1 = new Connection(null, null);
        Connection connection2 = new Connection(null, null);
        server.addConnection(connection1);
        server.addConnection(connection2);
        // Send a message to all clients
        server.sendMessageToAllClients("Test message");

        // Validate that the message is received by clients
        List<Connection> connections = server.getConnections();
        for (Connection connection : connections) {
            assertEquals("Test message", connection.getReceivedMessage());
        }
        // Stop the server
        server.stop();
    }

    @Test
    public void testRemoveConnection() {
        // Create a dummy connection
        Connection connection = new Connection(null, null);
        server.addConnection(connection);
        server.removeConnection(connection);

        // Validate that the connection is removed
        assertFalse(server.getConnections().contains(connection));
    }
}
