package messenger.server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

class MockSocket extends Socket {
    private String sentMessage;

    public MockSocket() throws IOException {
        super();
    }

    @Override
    public OutputStream getOutputStream() {
        return new OutputStream() {
            @Override
            public void write(int b) {
                // Do nothing
            }

            @Override
            public void write(byte[] b) throws IOException {
                sentMessage = new String(b);
            }
        };
    }

    public String getSentMessage() {
        return sentMessage;
    }
}
