package messenger.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private ServerSocket serverSocket;
    private boolean serverRunning;
    private List<Connection> connections;

    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }

    public Server() {
        connections = new ArrayList<>();
    }

    public void start() {
        try {
            serverSocket = new ServerSocket(9999);
            serverRunning = true;
            System.out.println("Server started. Listening for connections...");

            while (serverRunning) {
                Socket clientSocket = serverSocket.accept();
                Connection connection = new Connection(clientSocket, this);
                connections.add(connection);
                connection.start();

                sendMessageToAllClients("New client connected: " + connection.getCustomerName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Close the server socket when the server stops
            try {
                if (serverSocket != null && !serverSocket.isClosed()) {
                    serverSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        serverRunning = false;

        // Close all connections and stop the server
        for (Connection connection : connections) {
            connection.interrupt(); // Interrupt the connection thread to stop it
        }
        connections.clear();

        System.out.println("Server stopped.");
    }

    public void removeConnection(Connection connection) {
        connections.remove(connection);
        sendMessageToAllClients("Client disconnected: " + connection.getCustomerName());
    }

    public void sendMessageToAllClients(String message) {
        for (Connection connection : connections) {
            connection.sendMessage(message);
        }
    }

    public boolean isServerRunning() {
        return serverRunning;
    }

    public void addConnection(Connection connection) {
        connections.add(connection);
    }

    public List<Connection> getConnections() {
        return connections;
    }
}
