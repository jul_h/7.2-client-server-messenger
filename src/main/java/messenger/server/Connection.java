package messenger.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;

public class Connection extends Thread {

    private Socket clientSocket;
    private String customerName;
    private LocalDateTime connectionTime;
    private Server server;

    private BufferedReader reader;
    private PrintWriter writer;

    private String receivedMessage;

    public Connection(Socket clientSocket, Server server) {
        this.clientSocket = clientSocket;
        this.server = server;
        connectionTime = LocalDateTime.now();
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getReceivedMessage() {
        return receivedMessage;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setReceivedMessage(String receivedMessage) {
        this.receivedMessage = receivedMessage;
    }

    public void run() {
        try {
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            writer = new PrintWriter(clientSocket.getOutputStream(), true);

            // Receive customer name from client
            customerName = reader.readLine();
            System.out.println("New client connected: " + customerName);
            server.sendMessageToAllClients("New client connected: " + customerName);

            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                if (inputLine.equals("close")) {
                    break;
                }
                System.out.println(customerName + ": " + inputLine);
                server.sendMessageToAllClients(customerName + ": " + inputLine);
                receivedMessage = inputLine;
            }

            server.removeConnection(this);

            reader.close();
            writer.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        writer.println(message);
    }
}
