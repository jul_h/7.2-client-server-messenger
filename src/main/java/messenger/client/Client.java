package messenger.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private final WriterHolder writerHolder;
    private final ReaderHolder readerHolder;
    private final MessageReceiver messageReceiver;

    public Client(WriterHolder writerHolder, ReaderHolder readerHolder, MessageReceiver messageReceiver) {
        this.writerHolder = writerHolder;
        this.readerHolder = readerHolder;
        this.messageReceiver = messageReceiver;
    }

    public void connectToServer(String hostname, int port, String username) throws IOException {
        try {
            Socket clientSocket = new Socket(hostname, port);
            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            writerHolder.setWriter(writer);
            readerHolder.setReader(reader);

            writer.println(username);

            messageReceiver.startReceiving();

            BufferedReader userInputReader = new BufferedReader(new InputStreamReader(System.in));
            String userInput;
            do {
                userInput = userInputReader.readLine();
                writer.println(userInput);
            } while (!userInput.equals("close"));

            reader.close();
            writer.close();
            clientSocket.close();
        } catch (IOException e) {
            throw e;
        }
    }

    public MessageReceiver getMessageReceiver() {
        return messageReceiver;
    }

    public static void main(String[] args) {
        try {
            WriterHolder writerHolder = new WriterHolder();
            ReaderHolder readerHolder = new ReaderHolder();

            //Create the BufferedReader and pass it to the MessageReceiver
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            MessageReceiver messageReceiver = new MessageReceiver(reader);

            Client client = new Client(writerHolder, readerHolder, messageReceiver);
            client.connectToServer("localhost", 9999, "John");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}