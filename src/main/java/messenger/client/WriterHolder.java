package messenger.client;

import java.io.PrintWriter;

public class WriterHolder {
    private PrintWriter writer;

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }
}
