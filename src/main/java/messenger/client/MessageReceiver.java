package messenger.client;

import java.io.BufferedReader;
import java.io.IOException;

public class MessageReceiver {
    private BufferedReader reader;
    private String message;

    public MessageReceiver (BufferedReader reader) {
        this.reader = reader;
    }

    public void startReceiving() {
        new Thread(() -> {
            try {
                String incomingMessage;
                while ((incomingMessage = reader.readLine()) != null) {
                    System.out.println("Server: " + incomingMessage);
                    setMessage(incomingMessage);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
    public synchronized String getMessage() {
        return message;
    }
    private synchronized void setMessage(String message) {
        this.message = message;
    }
}
