package messenger.client;

import java.io.BufferedReader;

public class ReaderHolder {
    private BufferedReader reader;

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }
}
